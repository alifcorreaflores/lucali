function initRecordCSS(){
  $(".show-functions").show();
	$(".fixed-mobile-controls").show();
	$("#urlIframe").attr({"readonly":"true"});
	$("#link").attr({"class":"col-xs-10 col-sm-4 col-md-6 col-lg-6 col-xs-offset-1 col-md-offset-3"});
	$("#recordState").attr({"class":"btn btn-danger", "onClick":"finishRecord();"});
	$("#recordState").html('<span class="glyphicon glyphicon-stop"></span> Finalizar');
	$("#logo, #menu-desktop, #menu-mobile").hide();
	$(".navbar").css({ "padding": "5px" });
}

function maximizeIframe(){
	var elem = document.getElementById("iframeContent");
	if(elem.requestFullscreen) {elem.requestFullscreen();} else 
		if(elem.msRequestFullscreen){elem.msRequestFullscreen();} else 
			if(elem.mozRequestFullScreen){elem.mozRequestFullScreen();} else 
				if(elem.webkitRequestFullscreen){elem.webkitRequestFullscreen();}
}

function finishRecordCSS(){
	if(confirm("Deseja realmente finalizar a gravação? Tudo será perdido")){
			window.location.reload(false);
	}
}