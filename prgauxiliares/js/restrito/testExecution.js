createScript = "";
fieldHTML = "";
executionRealTime = "S";
amountInteractions = 0;
valueTime = 1500;

$(document).ready(function() {
	$("#urlIframe").keypress(function(elem) {
		var enter = 13;
		if (elem.keyCode == enter) {
			initRecord();
		}
	});

	setInterval(function() {
		if (document.getElementById('iframeContent').src != "") {

			$(document.getElementById('iframeContent')).ready(function() {
				setTimeout(function() {
					interactionElement();
				}, 100);
			});
			
		}
	}, 100);
	
	initRecord();
	
});

function usingRealTime(param) {
	executionRealTime = param;
}

function initRecord() {
	initRecordCSS();

	document.getElementById('iframeContent').src = "" + $("#urlIframe").val() + "";
	$(document.getElementById('iframeContent')).ready(function() {
		setTimeout(function() {
			interactionElement();
		}, 500);
	});
}

function interactionElement() {

	var htmlIframe = document.getElementById('iframeContent');
	var htmlUser = (htmlIframe.contentWindow || htmlUser.contentDocument);
	if (htmlUser.document) htmlUser = htmlUser.document;

	// Remover o target do HREF
	var lengthHref = htmlUser.body.getElementsByTagName('a').length;
	for (var i = 0; i < lengthHref; i++) {
		htmlUser.body.getElementsByTagName('a')[i].removeAttribute('target');
	}

	$(htmlUser).mouseover(function(elem) {

		if (amountInteractions == 1) {
			createScript += "function script(){ \n";
		}
		if (elem.toElement.tagName == "INPUT") {

			var elementoTipo = elem.toElement.getAttribute("type");

			if (elementoTipo == "text") {
				$(elem.toElement).keypress(function() {

					var fieldSelect = elem.toElement;
					var attributeId = fieldSelect.getAttribute("id");
					
					if (fieldHTML != elem.toElement.value) {

						fieldHTML = elem.toElement.value;
						
						//document.getElementsByTagName('input')[2].getAttribute('name');
						if (executionRealTime == "S") {
							if(attributeId == null){
								console.log(elem.toElement.getAttribute('name'));
							}
							createScript += "setTimeout(function(){ $('#" + attributeId + "').val('" + elem.toElement.value + "'); }, " + valueTime + "); \n";
							valueTime = parseInt(valueTime) + parseInt(100);
						} else {
							createScript += "$('#" + attributeId + "').val('" + elem.toElement.value + "'); \n";
						}

					}

				});

				$(elem.toElement).blur(function() {

					var fieldSelect = elem.toElement;
					var attributeId = fieldSelect.getAttribute("id");

					if (fieldHTML != elem.toElement.value) {
						fieldHTML = elem.toElement.value;
						if (executionRealTime == "S") {
							createScript += "setTimeout(function(){ $('#" + attributeId + "').val('" + elem.toElement.value + "'); }, " + valueTime + "); \n";
							valueTime = parseInt(valueTime) + parseInt(100);
						} else {
							createScript += "$('#" + attributeId + "').val('" + elem.toElement.value + "'); \n";
						}
					}

				});

			} else if (elementoTipo == "submit") {

				$(elem.toElement).click(function() {
					// elem.toElement.setAttribute("disabled", true);
					elem.toElement.setAttribute("onClick", "script2();");
					var form = htmlUser.documentElement.getElementsByTagName('form')[0];
					var urlForm = form.getAttribute('action');
					form.setAttribute('action', urlForm + "?info=script2();");
					createScript += "\n /* Começa função para uma página seguida de um formulário *//* \n script2(); \n";
				});

			}

		}

		$("#console").html(createScript);
		amountInteractions++;

	});

}

function finishRecord() {
	finishRecordCSS();
	createScript += "}";
	$("#console").html(createScript);
}