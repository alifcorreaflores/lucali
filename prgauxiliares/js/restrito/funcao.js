﻿criarScript = "";
validarValorInput = "";
quantidadeIteracoes = 0;
campoHTML = "";
valorTempo = 1500;
executarTempoReal = "S";

$(document).ready(function(){
	var htmlIframe = document.getElementById('iframeContent');
    var htmlUsuario = (htmlIframe.contentWindow || htmlUsuario.contentDocument);
    if (htmlUsuario.document) htmlUsuario = htmlUsuario.document;
});

function usarTempoReal(param){
	executarTempoReal = param;
}

function initRecord() {

	$("#url").attr("class", "col-xs-7 col-sm-6 col-md-5 col-lg-5");
	$(".show-functions").show();
	$("#recordState").attr({"class":"btn btn-danger", "onClick":"finishRecord();"});
	$("#recordState").html ('<span class="glyphicon glyphicon-stop"></span> Finalizar');
	$('#repository').hide();
	$('#repository-mobile').hide();
 		document.getElementById('iframeContent').src = "" + $("#urlIframe").val() + "";
	$(document.getElementById('iframeContent')).ready(function() {
		setTimeout(function() {
			interacoesElementos();
		}, 100);
	});

	window.onbeforeunload = function() {
	    return "Are you sure?";
	 };
}

function finishRecord(){
	if(confirm("Deseja realmente finalizar a gravação? Tudo será perdido")){
			window.location.reload(false);
	}
}

function maximizeIframe() {
  $(".hide-functions").hide();
	$("#scale").attr("onClick","minimizeIframe();");
	$("#scale").html("<span class='glyphicon glyphicon-resize-small'></span> Tela Normal</button>");
	$(".embed-responsive-item").css({
		"height": "78%"
	});
}

function minimizeIframe() {
	$(".hide-functions").show();
	$("#scale").html("<span class='glyphicon glyphicon-fullscreen'></span> Tela Cheia</button>");
	$("#scale").attr("onClick","maximizeIframe();");
}

/*function interacoesElementos() {

    var htmlIframe = document.getElementById('iframeContent');
    var htmlUsuario = (htmlIframe.contentWindow || htmlUsuario.contentDocument);
    if (htmlUsuario.document) htmlUsuario = htmlUsuario.document;

    $(htmlUsuario).mouseover(function(elem) {

		if (quantidadeIteracoes == 1) {
			criarScript += "function script(){ \n";
		}

		if (elem.toElement.tagName == "INPUT") {

			var elementoTipo = elem.toElement.getAttribute("type");

			if(elementoTipo == "text"){

            $(elem.toElement).keypress(function() {
				var campoSelecionado = elem.toElement;
				var atributoId = campoSelecionado.getAttribute("id");

				if(campoHTML != elem.toElement.value){
					campoHTML = elem.toElement.value;
					if(executarTempoReal == "S"){
						criarScript += "setTimeout(function(){ $('#"+ atributoId +"').val('"+ elem.toElement.value +"'); }, "+ valorTempo +"); \n";
						valorTempo = parseInt(valorTempo) + parseInt(100);
					}else{
						criarScript += "$('#"+ atributoId +"').val('"+ elem.toElement.value +"'); \n";
					}
				}
            });

			$(elem.toElement).blur(function() {
				var campoSelecionado = elem.toElement;
				var atributoId = campoSelecionado.getAttribute("id");

				if(campoHTML != elem.toElement.value){
					campoHTML = elem.toElement.value;
					if(executarTempoReal == "S"){
						criarScript += "setTimeout(function(){ $('#"+ atributoId +"').val('"+ elem.toElement.value +"'); }, "+ valorTempo +"); \n";
						valorTempo = parseInt(valorTempo) + parseInt(100);
					}else{
						criarScript += "$('#"+ atributoId +"').val('"+ elem.toElement.value +"'); \n";
					}
				}
            });

			}else if(elementoTipo == "submit"){
				$(elem.toElement).click(function(){
					//elem.toElement.setAttribute("disabled", true);
					elem.toElement.setAttribute("onClick", "script2();");
					var formulario = htmlUsuario.documentElement.getElementsByTagName('form')[0];
					var urlFormulario = formulario.getAttribute('action');
					formulario.setAttribute('action', urlFormulario+"?informacao=script2();");

					//criarScript += "\n /* Começa função para uma página seguida de um formulário *//* \n script2(); \n";
				});
			}

        }

        $("#imprimir").html(criarScript);
        quantidadeIteracoes++;

    });

}

/*function finalizarExecucao(){

	criarScript += "}";

	$("#imprimir").html(criarScript);
}*/
