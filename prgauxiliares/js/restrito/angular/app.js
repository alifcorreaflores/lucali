var lucaliApp = angular.module('lucaliApp', ['ngRoute']);

lucaliApp.config(function($routeProvider, $locationProvider){

   $routeProvider
   .when('/', {
      templateUrl : 'criacao/criacao.html'
   })
   .when('/repositorio', {
      templateUrl : 'repositorio/repositorio.html'
   })
   .otherwise ({ redirectTo: '/' });

	//$locationProvider.html5Mode(true);

});
