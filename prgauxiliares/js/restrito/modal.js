function abrirModal(modal){

	if(modal == "configuracao"){
		$.post("../restrito/modalConfiguracoes.html", function(retorno){
			$("#conteudoModal").html(retorno);
		});
	}else if(modal == "projeto"){
		$.post("../restrito/criacao/modalTemplate.html", function(retorno){
			$("#conteudoModal").html(retorno);
			alterarConteudoModal('listaProjetos');
		});
	}else if(modal == "compartilharProjeto"){
		$.post("../restrito/repositorio/modalCompartilharProjeto.html", function(retorno){
			$("#conteudoModal").html(retorno);
		});
	}else if(modal == "excluirTeste"){
		$.post("../restrito/repositorio/modalExcluirTeste.html", function(retorno){
			$("#conteudoModal").html(retorno);
		});
	}

}

function alterarConteudoModal(conteudo){

	if(conteudo == "listaProjetos"){
		$.post("../restrito/criacao/modalProjeto.html", function(retorno){
			$("#conteudoProjetoModal").html(retorno);
		});
	}else if(conteudo == "novoProjeto"){
		$.post("../restrito/criacao/modalNovoProjeto.html", function(retorno){
			$("#conteudoProjetoModal").html(retorno);
		});
	}else if(conteudo == "novaPasta"){
		$.post("../restrito/criacao/modalNovaPasta.html", function(retorno){
			$("#conteudoProjetoModal").html(retorno);
		});
	}else if(conteudo == "novoTeste"){
		$.post("../restrito/criacao/modalNovoTeste.html", function(retorno){
			$("#conteudoProjetoModal").html(retorno);
		});
	}else if(conteudo == "salvarTeste"){
		$.post("../restrito/criacao/modalTesteSalvo.html", function(retorno){
			$("#tituloModal").html("");
			$("#conteudoProjetoModal").html(retorno);
			$("#nmProjeto").html("Teste 123");
		});
	}else if(conteudo == "desativarConta"){
		$.post("../restrito/modalDesativarConta.html", function(retorno){
			$("#tituloModal").html("Desativar meu Cadastro");
			$("#conteudoProjetoModal").html(retorno);
		});
	}else if(conteudo == "salvarCompartilhamento"){
		$.post("../restrito/repositorio/modalCompartilhamentoSalvo.html", function(retorno){
			$("#tituloModal").html("");
			$("#conteudoProjetoModal").html(retorno);
		});
	}else if(conteudo == "exclusaoTeste"){
		$.post("../restrito/repositorio/modalTesteExcluido.html", function(retorno){
			$("#tituloModal").html("Teste Excluído");
			$("#conteudoProjetoModal").html(retorno);
		});
	}

}
