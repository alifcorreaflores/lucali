# Tutorial para baixar e enviar os arquivos #

* git config --global http.proxy http://alif_flores:alif2012@10.3.79.253:3128 ; cd C:/xampp/htdocs ; git clone https://alifcorreaflores@bitbucket.org/alifcorreaflores/lucali.git ; cd lucali ; git checkout Development

* $ git config --global http.proxy http://alif_flores:alif2012@10.3.79.253:3128 ; cd C:/xampp/htdocs ; git clone https://lkruger1@bitbucket.org/alifcorreaflores/lucali.git ; cd lucali ; git checkout Development

Criar uma nova branch para fazer as alterações necessárias: **git checkout -b nomebranch**

# Setar o usuário Global #
* git config --global user.name "Alif Corrêa" ; git config --global user.email "alifcorrea@hotmail.com" ; git status 

* git config --global user.name "lkruger1"; git config --global user.email "lucas_kruger1@estudante.sc.senai.br" ; git status 

# Adicionar os arquivos #
* **git add .** 

O ponto (.) no fim do comando seleciona todos os arquivos modificados, caso queira adicionar apenas um arquivo digite "**git add nomearquivo.extensao**"

# Criar um commit #
* **git commit -m "comentario"**

Faça o commit com um comentário explicando o que foi feito. Ex: Correção do bug menu. Comando: 

# Enviar as alterações #
* **git push origin nomebranch**

Sobe a branch criada para o bitbucket

# Informações relevantes #
* Sempre que terminar suas atividades faça um commit e de um push para o git.
* Obter a ultima atualização da branch no github: git push